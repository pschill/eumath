namespace EuMathBenchmarks;

using BenchmarkDotNet.Attributes;
using EuMath;
using System.Collections.Generic;
using System.Numerics;

public class PrimesBenchmarks
{
    private ulong LittleN;
    private BigInteger BigN;

    [Params("123", "123456", "123456789", "123456789123456789", "123456789123456789123")]
    public string N;

    [GlobalSetup]
    public void Setup()
    {
        ulong.TryParse(N, out LittleN);
        BigN = BigInteger.Parse(N);
    }

    [Benchmark]
    public List<PrimeFactor<ulong>> PrimeFactors() => Primes.PrimeFactors(LittleN);

    [Benchmark]
    public List<PrimeFactor<BigInteger>> PrimeFactorsBig() => Primes.PrimeFactorsBig(BigN);
}
