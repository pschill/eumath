﻿namespace EuMath;

using System;
using System.Collections.Generic;

/// <summary>
/// Contains functions related to combinatorics.
/// </summary>
public static class Combinatorics
{
    /// <summary>
    /// Permutes the given list into the next permutation, where the set of all permutations is ordered
    /// lexicographically. Returns true if such a permutation exists. If no such permutation exists, this function
    /// transforms the range into the lexicographically first permutation and returns false.
    /// </summary>
    /// <typeparam name="T">The type of the compared objects.</typeparam>
    /// <param name="values">The values. If this is null, the function returns false.</param>
    /// <param name="comparer">The comparer. If this is null, <see cref="Comparer{T}.Default"/> is used instead.</param>
    /// <returns>True if a next permutation exists, otherwise false.</returns>
    /// <exception cref="ArgumentNullException">values is null.</exception>
    public static bool NextPermutation<T>(IList<T>? values, IComparer<T>? comparer = null)
    {
        if (values == null || values.Count < 2)
        {
            return false;
        }
        comparer ??= Comparer<T>.Default;
        var n = values.Count;
        var i = n - 1;
        while (true)
        {
            var i1 = i;
            if (comparer.Compare(values[--i], values[i1]) < 0)
            {
                var i2 = n;
                while (comparer.Compare(values[i], values[--i2]) >= 0)
                { }
                ListUtility.Swap(values, i, i2);
                ListUtility.Reverse(values, i1, n - i1);
                return true;
            }
            if (i == 0)
            {
                ListUtility.Reverse(values, 0, n);
                return false;
            }
        }
    }
}
