﻿namespace EuMath;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

/// <summary>
/// Contains functions related to prime numbers.
/// </summary>
public static class Primes
{
    /// <summary>
    /// Returns whether the given number is prime.
    /// </summary>
    /// <param name="number">The number.</param>
    /// <returns>True if the number is prime, otherwise false.</returns>
    public static bool IsPrime(ulong number)
    {
        if (number <= 1)
        {
            return false;
        }
        if (number <= 3)
        {
            return true;
        }
        if (number % 2 == 0 || number % 3 == 0)
        {
            return false;
        }
        var max = (ulong)Math.Sqrt(number);
        for (ulong i = 5; i <= max; i += 6)
        {
            if (number % i == 0 || number % (i + 2) == 0)
            {
                return false;
            }
        }
        return true;
    }

    /// <summary>
    /// Returns whether the given number is prime.
    /// </summary>
    /// <param name="number">The number.</param>
    /// <returns>True if the number is prime, otherwise false.</returns>
    /// <remarks>Returns false if the number is less than zero.</remarks>
    public static bool IsPrimeBig(BigInteger number)
    {
        if (number <= 1)
        {
            return false;
        }
        if (number <= 3)
        {
            return true;
        }
        if (number % 2 == 0 || number % 3 == 0)
        {
            return false;
        }
        for (var i = new BigInteger(5); i * i <= number; i += 6)
        {
            if (number % i == 0 || number % (i + 2) == 0)
            {
                return false;
            }
        }
        return true;
    }

    /// <summary>
    /// Computes the prime factors of the given number.
    /// </summary>
    /// <remarks>
    /// Here, the prime factors of a number n are all pairs (p, e) where p is prime and e > 0 is the largest
    /// integer such that p^e divides n.
    /// </remarks>
    /// <returns>
    /// All prime factors of the given number, or an empty list if the number is exactly 1.
    /// </returns>
    /// <exception cref="ArgumentException">number is less than 1.</exception>
    public static List<PrimeFactor<ulong>> PrimeFactors(ulong number)
    {
        if (number < 1)
        {
            throw new ArgumentException("Number must not be less than 1.", nameof(number));
        }
        var factors = new List<PrimeFactor<ulong>>();
        PrimeFactorsDivide(2, ref number, factors);
        PrimeFactorsDivide(3, ref number, factors);
        var max = (ulong)Math.Sqrt(number);
        for (ulong primeCandidate = 5; primeCandidate <= number; primeCandidate += 6)
        {
            if (primeCandidate > max)
            {
                factors.Add(new(number, 1));
                break;
            }
            PrimeFactorsDivide(primeCandidate, ref number, factors);
            PrimeFactorsDivide(primeCandidate + 2, ref number, factors);
            if (number == 1)
            {
                break;
            }
        }
        return factors;
    }

    /// <summary>
    /// Computes the prime factors of the given number.
    /// </summary>
    /// <remarks>
    /// Here, the prime factors of a number n are all pairs (p, e) where p is prime and e > 0 is the largest
    /// integer such that p^e divides n.
    /// </remarks>
    /// <returns>
    /// All prime factors of the given number, or an empty list if the number is exactly 1.
    /// </returns>
    /// <exception cref="ArgumentException">number is less than 1.</exception>
    public static List<PrimeFactor<BigInteger>> PrimeFactorsBig(BigInteger number)
    {
        if (number < 1)
        {
            throw new ArgumentException("Number must not be less than 1.", nameof(number));
        }
        var factors = new List<PrimeFactor<BigInteger>>();
        PrimeFactorsDivideBig(2, ref number, factors);
        PrimeFactorsDivideBig(3, ref number, factors);
        for (var primeCandidate = new BigInteger(5); primeCandidate <= number; primeCandidate += 6)
        {
            if (primeCandidate * primeCandidate > number)
            {
                factors.Add(new(number, 1));
                break;
            }
            PrimeFactorsDivideBig(primeCandidate, ref number, factors);
            PrimeFactorsDivideBig(primeCandidate + 2, ref number, factors);
            if (number == 1)
            {
                break;
            }
        }
        return factors;
    }

    /// <summary>
    /// Uses the sieve of Eratosthenes to compute all prime numbers less than the given bound. The returned
    /// BitArray has length <paramref name="bound"/>. The value at index i is true if i is prime and false if i is
    /// not prime.
    /// </summary>
    /// <param name="bound">The upper bound until the prime numbers are computed.</param>
    /// <returns>A BitArray where the value at index i is true if i is prime and false if i is not prime.</returns>
    /// <exception cref="ArgumentOutOfRangeException"><paramref name="bound"/> is less than zero.</exception>
    public static BitArray SieveEratosthenes(int bound)
    {
        if (bound < 0)
        {
            throw new ArgumentOutOfRangeException(nameof(bound), bound, "Bound must not be smaller than zero.");
        }
        if (bound < 2)
        {
            return new BitArray(bound, false);
        }
        var isPrime = new BitArray(bound, true);
        isPrime[0] = false;
        isPrime[1] = false;
        var max = (int)Math.Sqrt(bound);
        for (var i = 2; i <= max; ++i)
        {
            if (isPrime[i])
            {
                for (var j = i; j < bound - i; j += i)
                {
                    isPrime[j + i] = false;
                }
            }
        }
        return isPrime;
    }

    /// <summary>
    /// Computes the radical of the given number.
    /// </summary>
    /// <remarks>
    /// The radical of a number n is the product of all distinct prime numbers that divide n.
    /// </remarks>
    /// <param name="number">The number.</param>
    /// <returns>The radical of the given number.</returns>
    /// <exception cref="ArgumentException">number is less than 1.</exception>
    public static ulong FindRadical(ulong number)
    {
        if (number < 1)
        {
            throw new ArgumentException("Number must not be less than 1.", nameof(number));
        }
        ulong radical = 1;
        FindRadicalDivide(2, ref number, ref radical);
        FindRadicalDivide(3, ref number, ref radical);
        var max = (ulong)Math.Sqrt(number);
        for (ulong primeCandidate = 5; primeCandidate <= number; primeCandidate += 6)
        {
            if (primeCandidate > max)
            {
                radical *= number;
                break;
            }
            FindRadicalDivide(primeCandidate, ref number, ref radical);
            FindRadicalDivide(primeCandidate + 2, ref number, ref radical);
            if (number == 1)
            {
                break;
            }
        }
        return radical;
    }

    /// <summary>
    /// Uses a sieve to compute the radicals for all integers less than the given bound. The returned list has
    /// length <paramref name="bound"/>. The value at index i is the radical of i.
    /// </summary>
    /// <remarks>
    /// Since radicals are only defined for integers greater than zero, the value of the element at index 0 is
    /// unspecified.
    /// </remarks>
    /// <param name="bound">The upper bound until the radicals are computed.</param>
    /// <returns>A list where the value at index i is the radical of i.</returns>
    /// <exception cref="ArgumentOutOfRangeException"><paramref name="bound"/> is less than zero.</exception>
    public static List<int> SieveRadicals(int bound)
    {
        if (bound < 0)
        {
            throw new ArgumentOutOfRangeException(nameof(bound), bound, "Bound must not be smaller than zero.");
        }
        var radicals = Enumerable.Repeat(1, bound).ToList();
        if (bound < 2)
        {
            return radicals;
        }
        for (var i = 2; i < bound; ++i)
        {
            if (radicals[i] == 1)
            {
                for (var j = 0; j < bound - i; j += i)
                {
                    radicals[j + i] *= i;
                }
            }
        }
        return radicals;
    }

    private static void PrimeFactorsDivide(
        ulong prime,
        ref ulong number,
        List<PrimeFactor<ulong>> factors)
    {
        if (number % prime != 0)
        {
            return;
        }
        ulong exponent = 1;
        number /= prime;
        while (number % prime == 0)
        {
            ++exponent;
            number /= prime;
        }
        factors.Add(new(prime, exponent));
    }

    private static void PrimeFactorsDivideBig(
        BigInteger prime,
        ref BigInteger number,
        List<PrimeFactor<BigInteger>> factors)
    {
        if (number % prime != 0)
        {
            return;
        }
        ulong exponent = 1;
        number /= prime;
        while (number % prime == 0)
        {
            ++exponent;
            number /= prime;
        }
        factors.Add(new(prime, exponent));
    }

    private static void FindRadicalDivide(
        ulong prime,
        ref ulong number,
        ref ulong radical)
    {
        if (number % prime != 0)
        {
            return;
        }
        radical *= prime;
        number /= prime;
        while (number % prime == 0)
        {
            number /= prime;
        }
    }
}
