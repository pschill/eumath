﻿namespace EuMath;

using System;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Holds precomputed data with the primality of some numbers to speed up checking whether a number is prime.
/// </summary>
public class SievedPrimeChecker : IPrimeChecker
{
    private readonly BitArray _sieve;
    private readonly List<int> _primes;

    /// <summary>
    /// Constructs the SievedPrimeChecker and precomputes the primality for all numbers less than the given bound.
    /// </summary>
    /// <param name="bound">The upper bound until the prime numbers are computed.</param>
    /// <exception cref="ArgumentOutOfRangeException"><paramref name="bound"/> is less than zero.</exception>
    public SievedPrimeChecker(int bound)
    {
        _sieve = Primes.SieveEratosthenes(bound);
        _primes = new List<int>();
        for (var i = 0; i < _sieve.Count; ++i)
        {
            if (_sieve[i])
            {
                _primes.Add(i);
            }
        }
    }

    /// <summary>
    /// Checks whether the given number is prime.
    /// </summary>
    /// <param name="number">The number.</param>
    /// <returns>True if the number is prime, otherwise false.</returns>
    /// <remarks>
    /// If the number is less than the bound from the constructor, the result is retrieved by an array lookup.
    /// Otherwise, if the square root of the number is less than the bound from the constructor, a list of
    /// precomputed primes is used to check whether any prime divides the given number. Otherwise (if the square
    /// root of the number is equal to or greater than the bound from the constructor), this function just calls
    /// <see cref="Primes.IsPrime(ulong)"/>.
    /// </remarks>
    public bool IsPrime(ulong number)
    {
        var bound = (ulong)_sieve.Count;
        if (number < bound)
        {
            return _sieve[(int)number];
        }
        var sqrt = (ulong)Math.Sqrt(number);
        if (sqrt < bound)
        {
            for (var i = 0; i < _primes.Count; ++i)
            {
                var p = (ulong)_primes[i];
                if (number <= p)
                {
                    return true;
                }
                if (number % p == 0)
                {
                    return false;
                }
            }
            return true;
        }
        return Primes.IsPrime(number);
    }
}
