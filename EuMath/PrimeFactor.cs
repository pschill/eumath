namespace EuMath;

/// <summary>
/// Pair of a prime number and its exponent.
/// </summary>
public record PrimeFactor<T>(T Prime, ulong Exponent);
