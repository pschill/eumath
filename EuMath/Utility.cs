﻿namespace EuMath;

/// <summary>
/// Contains common utility functions.
/// </summary>
public static class Utility
{
    /// <summary>
    /// Returns the greatest common divisor of a and b.
    /// </summary>
    /// <param name="a">First number.</param>
    /// <param name="b">Second number.</param>
    /// <returns>Greatest common divisor of a and b.</returns>
    public static ulong Gcd(ulong a, ulong b)
    {
        return b == 0 ? a : Gcd(b, a % b);
    }
}
