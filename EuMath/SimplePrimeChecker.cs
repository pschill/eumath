﻿namespace EuMath;

/// <summary>
/// Uses a simple algorithm to check whether numbers are prime.
/// </summary>
public class SimplePrimeChecker : IPrimeChecker
{
    /// <summary>
    /// Uses <see cref="Primes.IsPrime(ulong)"/> to check whether the given number is prime.
    /// </summary>
    /// <param name="number">The number.</param>
    /// <returns>True if the number is prime, otherwise false.</returns>
    public bool IsPrime(ulong number)
    {
        return Primes.IsPrime(number);
    }
}
