﻿namespace EuMath;

using System.Collections.Generic;
using System.Diagnostics;

/// <summary>
/// Contains utility functions for lists.
/// </summary>
internal class ListUtility
{
    /// <summary>
    /// Reverses the given range.
    /// </summary>
    /// <typeparam name="T">The object type.</typeparam>
    /// <param name="values">The list.</param>
    /// <param name="index">The zero-based index of the range.</param>
    /// <param name="count">The length of the range.</param>
    internal static void Reverse<T>(IList<T> values, int index, int count)
    {
        Debug.Assert(values != null);
        Debug.Assert(index >= 0);
        Debug.Assert(count >= 0);
        Debug.Assert(index + count <= values.Count);
        for (var i = 0; i < count / 2; ++i)
        {
            Swap(values, index + i, index + count - i - 1);
        }
    }

    /// <summary>
    /// Swaps the elements at the given indices.
    /// </summary>
    /// <typeparam name="T">The object type.</typeparam>
    /// <param name="values">The list.</param>
    /// <param name="firstIndex">The index of the first swapped element.</param>
    /// <param name="secondIndex">The index of the second swapped element.</param>
    internal static void Swap<T>(IList<T> values, int firstIndex, int secondIndex)
    {
        Debug.Assert(values != null);
        Debug.Assert(firstIndex >= 0);
        Debug.Assert(firstIndex < values.Count);
        Debug.Assert(secondIndex >= 0);
        Debug.Assert(secondIndex < values.Count);
        (values[firstIndex], values[secondIndex]) = (values[secondIndex], values[firstIndex]);
    }
}
