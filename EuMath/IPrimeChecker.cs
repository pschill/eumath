﻿namespace EuMath;

/// <summary>
/// Checks whether numbers are prime.
/// </summary>
public interface IPrimeChecker
{
    /// <summary>
    /// Returns whether the given number is prime.
    /// </summary>
    /// <param name="number">The number.</param>
    /// <returns>True if the number is prime, otherwise false.</returns>
    bool IsPrime(ulong number);
}
