﻿namespace EuMathTests;

using EuMath;
using FluentAssertions;
using System.Linq;
using Xunit;

public class CombinatoricsTests
{
    [Theory]
    [InlineData("", "", false)]
    [InlineData("a", "a", false)]
    [InlineData("ab", "ba", true)]
    [InlineData("ba", "ab", false)]
    [InlineData("aab", "aba", true)]
    [InlineData("aba", "baa", true)]
    [InlineData("baa", "aab", false)]
    [InlineData("abc", "acb", true)]
    [InlineData("acb", "bac", true)]
    [InlineData("bac", "bca", true)]
    [InlineData("bca", "cab", true)]
    [InlineData("cab", "cba", true)]
    [InlineData("cba", "abc", false)]
    public void NextPermutation_ProducesTheCorrectPermutation(string input, string expectedPermutation, bool expectedReturnValue)
    {
        var l = input.ToList();

        var actualReturnValue = Combinatorics.NextPermutation(l);

        var actualPermutation = string.Concat(l);
        actualReturnValue.Should().Be(expectedReturnValue);
        actualPermutation.Should().Be(expectedPermutation);
    }

    [Fact]
    public void NextPermutation_ListIsNull_ReturnsFalse()
    {
        var actualResult = Combinatorics.NextPermutation<int>(null);

        actualResult.Should().BeFalse();
    }
}
