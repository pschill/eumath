﻿namespace EuMathTests;

using EuMath;
using FluentAssertions;
using System.Collections.Generic;
using Xunit;

public class UtilityTests
{
    [Theory]
    [MemberData(nameof(GetGcdTestData))]
    public void Gcd_ReturnsCorrectResult(ulong a, ulong b, ulong expectedGcd)
    {
        var actualGcd = Utility.Gcd(a, b);

        actualGcd.Should().Be(expectedGcd);

        // Check symmetry.
        var otherActualGcd = Utility.Gcd(b, a);
        otherActualGcd.Should().Be(expectedGcd);
    }

    public static IEnumerable<object[]> GetGcdTestData()
    {
        foreach (var (first, second, gcd) in UtilityTestData.GetGcdOfExampleValues())
        {
            yield return new object[] { first, second, gcd };
        }
    }
}
