﻿namespace EuMathTests;

using System.Collections.Generic;

public static class UtilityTestData
{
    public static IEnumerable<(ulong First, ulong Second, ulong Gcd)> GetGcdOfExampleValues()
    {
        for (ulong i = 0; i <= 9; ++i)
        {
            yield return (0, i, i);
        }
        for (ulong i = 1; i <= 9; ++i)
        {
            yield return (1, i, 1);
        }
        for (ulong i = 2; i <= 9; ++i)
        {
            yield return (i, i, i);
        }
        yield return (2, 3, 1);
        yield return (2, 4, 2);
        yield return (2, 5, 1);
        yield return (2, 6, 2);
        yield return (2, 7, 1);
        yield return (2, 8, 2);
        yield return (2, 9, 1);
        yield return (3, 4, 1);
        yield return (3, 5, 1);
        yield return (3, 6, 3);
        yield return (3, 7, 1);
        yield return (3, 8, 1);
        yield return (3, 9, 3);
        yield return (4, 5, 1);
        yield return (4, 6, 2);
        yield return (4, 7, 1);
        yield return (4, 8, 4);
        yield return (4, 9, 1);
        yield return (5, 6, 1);
        yield return (5, 7, 1);
        yield return (5, 8, 1);
        yield return (5, 9, 1);
        yield return (6, 7, 1);
        yield return (6, 8, 2);
        yield return (6, 9, 3);
        yield return (7, 8, 1);
        yield return (7, 9, 1);
        yield return (8, 9, 1);
        yield return (18395377425, 11086661061, 1901331);
    }
}
