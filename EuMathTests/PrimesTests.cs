namespace EuMathTests;

using EuMath;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using Xunit;

public class PrimesTests
{
    [Theory]
    [MemberData(nameof(GetIsPrimeTestData))]
    public void IsPrime_ReturnsCorrectResult(ulong number, bool expectedIsPrime)
    {
        Primes.IsPrime(number).Should().Be(expectedIsPrime);
    }

    [Theory]
    [MemberData(nameof(GetIsPrimeTestData))]
    public void IsPrimeBig_ReturnsCorrectResult(ulong number, bool expectedIsPrime)
    {
        var bigNumber = new BigInteger(number);
        Primes.IsPrimeBig(bigNumber).Should().Be(expectedIsPrime);
    }

    public static IEnumerable<object[]> GetIsPrimeTestData()
    {
        var firstHundred = PrimeTestData.GetPrimalityOfFirstHundredIntegers().ToList();
        for (var i = 0; i < firstHundred.Count; ++i)
        {
            yield return new object[] { (ulong)i, firstHundred[i] };
        }
        var large = TestConfig.Get().RunLongRunningTests
            ? PrimeTestData.GetPrimalityOfLargeIntegers().Concat(PrimeTestData.GetPrimalityOfReallyLargeIntegers())
            : PrimeTestData.GetPrimalityOfLargeIntegers();
        foreach (var (number, isPrime) in large)
        {
            yield return new object[] { number, isPrime };
        }
    }

    [Fact]
    public void PrimeFactors_InputIsZero_ThrowsArgumentException()
    {
        Action primeFactors = () => Primes.PrimeFactors(0);

        primeFactors.Should().Throw<ArgumentException>();
    }

    [Theory]
    [MemberData(nameof(GetPrimeFactorsTestData))]
    public void PrimeFactors_ReturnsCorrectResult(ulong number, List<PrimeFactor<ulong>> expectedPrimeFactors)
    {
        var actualPrimeFactors = Primes.PrimeFactors(number);

        actualPrimeFactors.Should().Equal(expectedPrimeFactors);
    }

    public static IEnumerable<object[]> GetPrimeFactorsTestData()
    {
        var values = PrimeTestData.GetPrimeFactorsOfExampleIntegers();
        foreach (var (value, factors) in PrimeTestData.GetPrimeFactorsOfExampleIntegers())
        {
            yield return new object[] { value, factors.Select(pf => new PrimeFactor<ulong>(pf.Prime, pf.Exponent)).ToList() };
        }
        if (TestConfig.Get().RunLongRunningTests)
        {
            var greatestPrime = PrimeTestData.GetPrimalityOfReallyLargeIntegers().Where(o => o.IsPrime).Max().Value;
            yield return new object[] { greatestPrime, new List<PrimeFactor<ulong>> { new(greatestPrime, 1) } };
        }
    }

    [Theory]
    [InlineData(0)]
    [InlineData(-1)]
    [InlineData(-2)]
    [InlineData(-3)]
    public void PrimeFactorsBig_InputIsLessThanOne_ThrowsArgumentException(int number)
    {
        var bigNumber = new BigInteger(number);

        Action primeFactors = () => Primes.PrimeFactorsBig(bigNumber);

        primeFactors.Should().Throw<ArgumentException>();
    }

    [Theory]
    [MemberData(nameof(GetPrimeFactorsTestData))]
    public void PrimeFactorsBig_ReturnsCorrectResult(ulong number, List<PrimeFactor<ulong>> expectedPrimeFactors)
    {
        var bigNumber = new BigInteger(number);
        var expectedBigPrimeFactors = expectedPrimeFactors.Select(o => new PrimeFactor<BigInteger>(o.Prime, o.Exponent)).ToList();

        var actualPrimeFactors = Primes.PrimeFactorsBig(bigNumber);

        actualPrimeFactors.Should().Equal(expectedBigPrimeFactors);
    }

    [Theory]
    [InlineData(0)]
    [InlineData(1)]
    [InlineData(2)]
    [InlineData(3)]
    [InlineData(4)]
    [InlineData(5)]
    [InlineData(6)]
    [InlineData(7)]
    [InlineData(8)]
    [InlineData(9)]
    [InlineData(10)]
    [InlineData(25)]
    [InlineData(100)]
    public void SieveEratosthenes_ReturnsCorrectResult(int bound)
    {
        var expectedSieved = PrimeTestData.GetPrimalityOfFirstHundredIntegers().ToList();
        bound.Should().BeLessOrEqualTo(expectedSieved.Count);

        var actualSieved = Primes.SieveEratosthenes(bound);

        actualSieved.Count.Should().Be(bound);
        var actualSievedArray = new bool[actualSieved.Count];
        actualSieved.CopyTo(actualSievedArray, 0);
        actualSievedArray.Should().Equal(expectedSieved.Take(bound));
    }

    [Theory]
    [MemberData(nameof(GetLargeSieveTestData))]
    public void SieveEratosthenes_LargeSieve_DoesNotThrow(int bound)
    {
        var actualSieved = Primes.SieveEratosthenes(bound);

        actualSieved.Count.Should().Be(bound);
    }

    public static IEnumerable<object[]> GetLargeSieveTestData()
    {
        yield return new object[] { 65536 };
        if (TestConfig.Get().RunLongRunningTests)
        {
            yield return new object[] { int.MaxValue };
        }
    }

    [Theory]
    [InlineData(-1)]
    [InlineData(-2)]
    [InlineData(-3)]
    public void SieveEratosthenes_BoundIsLessThanZero_ThrowsArgumentOutOfRangeException(int bound)
    {
        Action sieveEratosthenes = () => Primes.SieveEratosthenes(bound);

        sieveEratosthenes.Should().Throw<ArgumentOutOfRangeException>();
    }

    [Theory]
    [MemberData(nameof(GetRadicalTestData))]
    public void FindRadical_ReturnsCorrectResult(ulong number, ulong expectedRadical)
    {
        var actualRadical = Primes.FindRadical(number);

        actualRadical.Should().Be(expectedRadical);
    }

    public static IEnumerable<object[]> GetRadicalTestData()
    {
        var firstHundred = PrimeTestData.GetRadicalsOfFirstHundredIntegers().ToList();
        for (var i = 1; i < firstHundred.Count; ++i)
        {
            yield return new object[] { (ulong)i, (ulong)firstHundred[i] };
        }
        foreach (var (number, radical) in PrimeTestData.GetRadicalsOfLargeIntegers())
        {
            yield return new object[] { number, radical };
        }
    }

    [Fact]
    public void FindRadical_InputIsZero_ThrowsArgumentException()
    {
        Action findRadical = () => Primes.FindRadical(0);

        findRadical.Should().Throw<ArgumentException>();
    }

    [Theory]
    [InlineData(0)]
    [InlineData(1)]
    [InlineData(2)]
    [InlineData(3)]
    [InlineData(4)]
    [InlineData(5)]
    [InlineData(6)]
    [InlineData(7)]
    [InlineData(8)]
    [InlineData(9)]
    [InlineData(10)]
    [InlineData(25)]
    [InlineData(100)]
    public void SieveRadicals_ReturnsCorrectResult(int bound)
    {
        var expectedRadicals = PrimeTestData.GetRadicalsOfFirstHundredIntegers().ToList();
        bound.Should().BeLessOrEqualTo(expectedRadicals.Count);

        var actualRadicals = Primes.SieveRadicals(bound);

        actualRadicals.Count.Should().Be(bound);
        actualRadicals.Skip(1).Should().Equal(expectedRadicals.Take(bound).Skip(1));
    }

    [Theory]
    [InlineData(-1)]
    [InlineData(-2)]
    [InlineData(-3)]
    public void SieveRadicals_BoundIsLessThanZero_ThrowsArgumentException(int bound)
    {
        Action sieveRadicals = () => Primes.SieveRadicals(bound);

        sieveRadicals.Should().Throw<ArgumentException>();
    }
}
