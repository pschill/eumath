﻿namespace EuMathTests;

using EuMath;
using FluentAssertions;
using System.Collections.Generic;
using System.Linq;
using Xunit;

public class SimplePrimeCheckerTests
{
    [Theory]
    [MemberData(nameof(GetIsPrimeTestData))]
    public void IsPrime_ReturnsCorrectResult(ulong number, bool expectedIsPrime)
    {
        var checker = new SimplePrimeChecker();

        var actualIsPrime = checker.IsPrime(number);

        actualIsPrime.Should().Be(expectedIsPrime);
    }

    public static IEnumerable<object[]> GetIsPrimeTestData()
    {
        var firstHundred = PrimeTestData.GetPrimalityOfFirstHundredIntegers().ToList();
        for (var i = 0; i < firstHundred.Count; ++i)
        {
            yield return new object[] { (ulong)i, firstHundred[i] };
        }
        foreach (var (number, isPrime) in PrimeTestData.GetPrimalityOfLargeIntegers())
        {
            yield return new object[] { number, isPrime };
        }
    }
}
