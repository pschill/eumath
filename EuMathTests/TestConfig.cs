﻿namespace EuMathTests;

using Microsoft.Extensions.Configuration;

public class TestConfig
{
    public bool RunLongRunningTests { get; set; }

    public static TestConfig Get()
    {
        var rawConfig = new ConfigurationBuilder()
            .AddEnvironmentVariables()
            .Build();
        var config = new TestConfig();
        rawConfig.GetSection("EuMath").Bind(config);
        return config;
    }
}
