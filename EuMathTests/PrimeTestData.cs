﻿namespace EuMathTests;

using System.Collections.Generic;

public static class PrimeTestData
{
    public static IEnumerable<bool> GetPrimalityOfFirstHundredIntegers()
    {
        return new[]
        {
            false, false, true, true, false, true, false, true, false, false,
            false, true, false, true, false, false, false, true, false, true,
            false, false, false, true, false, false, false, false, false, true,
            false, true, false, false, false, false, false, true, false, false,
            false, true, false, true, false, false, false, true, false, false,
            false, false, false, true, false, false, false, false, false, true,
            false, true, false, false, false, false, false, true, false, false,
            false, true, false, true, false, false, false, false, false, true,
            false, false, false, true, false, false, false, false, false, true,
            false, false, false, false, false, false, false, true, false, false
        };
    }

    public static IEnumerable<(ulong Value, bool IsPrime)> GetPrimalityOfLargeIntegers()
    {
        yield return (49968365, false);
        yield return (49968367, true);
        yield return (49968371, true);
        yield return (49968373, false);
        yield return (49968377, true);
        yield return (49968379, false);
        yield return (1099511627789, false);
        yield return (1099511627791, true);
        yield return (1099511627803, true);
        yield return (1099511627805, false);
        yield return (1099511627831, true);
        yield return (1099511627833, false);
    }

    public static IEnumerable<(ulong Value, bool IsPrime)> GetPrimalityOfReallyLargeIntegers()
    {
        yield return (18446744073709551557, true);
        yield return (18446744073709551559, false);
    }

    public static IEnumerable<(ulong Value, List<(ulong Prime, ulong Exponent)> PrimeFactors)> GetPrimeFactorsOfExampleIntegers()
    {
        yield return (1, new());
        yield return (2, new() { (2, 1) });
        yield return (3, new() { (3, 1) });
        yield return (4, new() { (2, 2) });
        yield return (5, new() { (5, 1) });
        yield return (6, new() { (2, 1), (3, 1) });
        yield return (7, new() { (7, 1) });
        yield return (8, new() { (2, 3) });
        yield return (9, new() { (3, 2) });
        yield return (10, new() { (2, 1), (5, 1) });
        yield return (11, new() { (11, 1) });
        yield return (12, new() { (2, 2), (3, 1) });
        yield return (13, new() { (13, 1) });
        yield return (14, new() { (2, 1), (7, 1) });
        yield return (15, new() { (3, 1), (5, 1) });
        yield return (16, new() { (2, 4) });
        yield return (17, new() { (17, 1) });
        yield return (18, new() { (2, 1), (3, 2) });
        yield return (19, new() { (19, 1) });
        yield return (20, new() { (2, 2), (5, 1) });
        yield return (21, new() { (3, 1), (7, 1) });
        yield return (22, new() { (2, 1), (11, 1) });
        yield return (23, new() { (23, 1) });
        yield return (24, new() { (2, 3), (3, 1) });
        yield return (25, new() { (5, 2) });
        yield return (26, new() { (2, 1), (13, 1) });
        yield return (27, new() { (3, 3) });
        yield return (28, new() { (2, 2), (7, 1) });
        yield return (29, new() { (29, 1) });
        yield return (30, new() { (2, 1), (3, 1), (5, 1) });
        yield return (35, new() { (5, 1), (7, 1) });
        yield return (60, new() { (2, 2), (3, 1), (5, 1) });
        yield return (5400, new() { (2, 3), (3, 3), (5, 2) });
        yield return (1139269212, new() { (2, 2), (3, 4), (17, 2), (23, 3) });
        yield return (4294967311, new() { (4294967311, 1) });
        yield return (18446744073709551615, new() { (3, 1), (5, 1), (17, 1), (257, 1), (641, 1), (65537, 1), (6700417, 1) });
    }

    public static IEnumerable<int> GetRadicalsOfFirstHundredIntegers()
    {
        return new[]
        {
            0, 1, 2, 3, 2, 5, 6, 7, 2, 3,
            10, 11, 6, 13, 14, 15, 2, 17, 6, 19,
            10, 21, 22, 23, 6, 5, 26, 3, 14, 29,
            30, 31, 2, 33, 34, 35, 6, 37, 38, 39,
            10, 41, 42, 43, 22, 15, 46, 47, 6, 7,
            10, 51, 26, 53, 6, 55, 14, 57, 58, 59,
            30, 61, 62, 21, 2, 65, 66, 67, 34, 69,
            70, 71, 6, 73, 74, 15, 38, 77, 78, 79,
            10, 3, 82, 83, 42, 85, 86, 87, 22, 89,
            30, 91, 46, 93, 94, 95, 6, 97, 14, 33
        };
    }

    public static IEnumerable<(ulong Value, ulong Radical)> GetRadicalsOfLargeIntegers()
    {
        yield return (5400, 30);
        yield return (1139269212, 2346);
        yield return (4294967311, 4294967311);
        yield return (18446744073709551615, 18446744073709551615);
    }
}
