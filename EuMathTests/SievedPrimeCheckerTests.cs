﻿namespace EuMathTests;

using EuMath;
using FluentAssertions;
using System.Collections.Generic;
using System.Linq;
using Xunit;

public class SievedPrimeCheckerTests
{
    [Theory]
    [InlineData(0)]
    [InlineData(1)]
    [InlineData(2)]
    [InlineData(3)]
    [InlineData(10)]
    [InlineData(11)]
    [InlineData(12)]
    [InlineData(13)]
    [InlineData(100)]
    public void IsPrime_ReturnsCorrectResult(int bound)
    {
        var primality = GetPrimalityTestData();
        var checker = new SievedPrimeChecker(bound);

        foreach (var (number, expectedIsPrime) in primality)
        {
            var actualIsPrime = checker.IsPrime(number);
            actualIsPrime.Should().Be(expectedIsPrime);
        }
    }

    private static IEnumerable<(ulong Value, bool IsPrime)> GetPrimalityTestData()
    {
        var firstHundred = PrimeTestData.GetPrimalityOfFirstHundredIntegers().ToList();
        for (var i = 0; i < firstHundred.Count; ++i)
        {
            yield return ((ulong)i, firstHundred[i]);
        }
        foreach (var (number, isPrime) in PrimeTestData.GetPrimalityOfLargeIntegers())
        {
            yield return (number, isPrime);
        }
    }
}
